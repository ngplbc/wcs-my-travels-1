import React from "react";
import Travel from "./Travel";

const Travels = ({ travelArr }) => {
  return <ul>{travelArr.map((travelObj, idx) => <Travel key={idx} idx={idx} travelObj={travelObj} />)}</ul>
}

export default Travels;