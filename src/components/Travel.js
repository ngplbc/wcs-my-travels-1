import React from "react";

const Travel = ({ idx, travelObj }) => {
  const { destination, country, photo, distance } = travelObj;

  return (
    <li>
      <h3>Travel #{idx + 1}</h3>
      <p>Destination: {destination}</p>
      <p>Country: {country}</p>
      <p>Distance: {distance} kms</p>
      <img src={photo} alt={destination} width="300px" height="300px"></img>
    </li>
  )
}

export default Travel;