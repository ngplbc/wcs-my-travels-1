import React, { useState } from "react";
import Travels from "./components/Travels";

const App = () => {
  const [travelArr, setTravel] = useState(
    [
      {
        destination: "Valleta",
        country: "Malta",
        photo: "https://www.newstatesman.com/sites/default/files/styles/lead_image/public/Longreads_2018/02/2018_08_malta_opener.jpg?itok=_-KXSZQe",
        distance: "4000"
      },
      {
        destination: "Madrid",
        country: "Spain",
        photo: "https://joaobarrigudo.pt/wp-content/uploads/2019/02/o-que-fazer-em-madrid.jpg",
        distance: "1000"
      },
      {
        destination: "Havana",
        country: "Cuba",
        photo: "https://nit.pt/wp-content/uploads/2018/09/c34cbd0084d36f9dda2b43430e17a719-754x394.jpg",
        distance: "300010"
      },
      {
        destination: "Paris",
        country: "France",
        photo: "https://static.independent.co.uk/s3fs-public/thumbnails/image/2019/08/07/08/paris.jpg ",
        distance: "2000"
      },
      {
        destination: "Buenos Aires",
        country: "Argentina",
        photo: "https://2.bp.blogspot.com/-DZpOBNp1gL8/V0YJEw_71BI/AAAAAAAACMI/ZUmIzxU3d387mB991YvHID5h2WeWYXZSgCKgB/s1600/Passagens-Aereas-Buenos-Aires.jpg",
        distance: "307000"
      }
    ]
  );

  return (
    <div className="App">
      <h3>My travels:</h3>
      <Travels travelArr={travelArr}/>
    </div>
  );
}

export default App;
